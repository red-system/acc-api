<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

//$router->get('/', function () use ($router) {
//
//    return $router->app->version();
//});



Route::group(['prefix' => 'v1', 'namespace' => 'General'], function () {

    Route::post('/login', 'Login@index');

    Route::group(['prefix' => 'hrd', 'middleware' => 'auth'], function () {
        Route::get('/dashboard', 'Dashboard@index');
    });

});
Route::group(['prefix' => 'v1/hrd', 'namespace' => 'Approval'], function () {
    Route::get('/approval', 'Approval@index');
});
Route::group(['prefix' => 'v1/hrd', 'namespace' => 'SOPManagement', 'middleware' => 'auth'], function () {
});

// acc
Route::group(['prefix' => 'v1', 'namespace' => 'Acc', 'middleware' => 'auth'], function () {
    //COA
    Route::get('/coa','Coa@index');
    Route::get('/coa/{id}','Coa@row');
    Route::post('/coa','Coa@store');
    Route::post('/coa/{id}','Coa@update');
    Route::delete('/coa/{id}','Coa@delete');
    Route::get('/coa-pembayaran','Coa@pembayaran');
    //AC Master
    Route::get('/master','Master@index');
    Route::get('/master/{id}','Master@row');
    Route::post('/master','Master@store');
    Route::post('/master/{id}','Master@update');
    Route::delete('/master/{id}','Master@delete');
    //AC Master Detail
    Route::get('/master-detail','MasterDetail@index');
    Route::get('/master-detail/{id}','MasterDetail@row');
    Route::post('/master-detail','MasterDetail@store');
    Route::post('/master-detail/{id}','MasterDetail@update');
    Route::delete('/master-detail/{id}','MasterDetail@delete');
    //Jurnal Umum
    Route::get('/jurnal-umum','JurnalUmum@index');
    Route::get('/jurnal-umum/{id}','JurnalUmum@row');
    Route::post('/jurnal-umum','JurnalUmum@store');
    Route::post('/jurnal-umum/{id}','JurnalUmum@update');
    Route::delete('/jurnal-umum/{id}','JurnalUmum@delete');

    //Transaksi
    Route::get('/transaksi','Transaksi@index');
    Route::get('/transaksi/{id}','Transaksi@row');
    Route::post('/transaksi','Transaksi@store');
    Route::post('/transaksi/{id}','Transaksi@update');
    Route::delete('/transaksi/{id}','Transaksi@delete');

});

