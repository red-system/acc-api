<?php

namespace App\Helpers;

use App\Models\mStaff;
use Illuminate\Support\Facades\Validator;
use App\Models\mLogActivity;

class General
{

    public static $login_success = 'Berhasil login';
    public static $validation_error = 'Isian field belum benar';
    public static $error = 'error';
    public static $success = 'success';
    public static $get = 'Berhasil mendapatkan data';
    public static $store = 'Berhasil menyimpan data';
    public static $update = 'Berhasil memperbarui data';
    public static $delete = 'Berhasil menghapus data';
    public static $import = 'Berhasil import file';
    public static $unavailable = 'Service belum tersedia';
    public static $already_absence = 'Data absensi telah tersedia';
    public static $date_format_view = 'd F Y H:i';


    /**
     * Generate token
     * @return string
     */
    public static function generate_token()
    {
        return bin2hex(openssl_random_pseudo_bytes(64));
    }

    /**
     * @param string $status
     * @param string $message
     * @param null $data
     * @param null $errors
     * @return array
     */
    public static function response($status = 'error', $message = 'Empty', $data = NULL, $errors = NULL)
    {
        return [
            'status' => $status,
            'message' => $message,
            'data' => $data,
            'errors' => $errors
        ];
    }

    public static function validator($request = [], $rules = [], $messages = [], $attributes = [])
    {
        $validator = Validator::make($request, $rules, $messages, $attributes);
        if ($validator->fails()) {
            $data = General::response(General::$error, General::$validation_error, NULL, $validator->errors());
            response()->json($data)->send();
            die();
        }
    }

    /**
     * @param $staff_id
     * @param $menu_label
     * @param $log_activity_title
     * @param $log_activity_description
     * @param $log_activity_type [get, insert, update, delete]
     */
    public static function log_activity($menu_label = '', $log_activity_title = '', $log_activity_description = '', $log_activity_type = '', $staff_id = NULL)
    {

        if ($staff_id == NULL) {
            $staff = General::user_detail();
            $staff_id = $staff->staff_id;
            $staff_name = $staff->staff_name;
        } else {
            $staff_name = mStaff::select('staff_name')->where('staff_id', $staff_id)->value('staff_name');
        }

        $data_insert = [
            'staff_id' => $staff_id,
            'staff_name' => $staff_name,
            'menu_label' => $menu_label,
            'log_activity_title' => $log_activity_title,
            'log_activity_description' => $log_activity_description,
            'log_activity_type' => $log_activity_type,
            'created_at' => date('Y-m-d H:i:s'),
        ];

        mLogActivity::insert($data_insert);
    }

    public static function user_detail()
    {
        if (isset(apache_request_headers()['Authorization'])) {
            $token_raw = apache_request_headers()['Authorization'];
            $token_arr = explode(' ', $token_raw);
            $token = $token_arr[1];
            $staff = mStaff::where('staff_token', $token)->first(['staff_id', 'staff_name']);
        } else {
            $staff = (object)['staff_id', 'staff_name'];
            $staff->staff_id = 0;
            $staff->staff_name = 'Guest';
        }

        return $staff;
    }

    public static function rename_filename_upload($extension)
    {
        return date('m-d-Y_H-i-s') . '__' . uniqid() . '.' . $extension;
    }

    public static function month_name_id($id)
    {
        $month = ["", "Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"];
        return $month[$id];
    }

    public static function user_role_data_init()
    {
        return '{"hrd":{"sop_management":{"sub_menu":{"1":{"label":"Pengaturan HRD","action":{"view":false,"update":false},"view":false},"2":{"label":"Profil Perusahaan","action":{"view":false,"update":false},"view":false},"3":{"label":"Struktur Organisasi","action":{"overview":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"4":{"label":"Deskripsi Pekerjaan","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"5":{"label":"Alur Kerja","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"6":{"label":"Penghargaan","action":{"panduan_list":false,"panduan_create":false,"panduan_edit":false,"panduan_delete":false,"reward_list":false,"reward_create":false,"reward_edit":false,"reward_delete":false},"view":false},"7":{"label":"Hukuman","action":{"panduan_list":false,"panduan_create":false,"panduan_edit":false,"panduan_delete":false,"punishment_list":false,"punishment_create":false,"punishment_edit":false,"punishment_delete":false},"view":false},"8":{"label":"Aturan & Regulasi","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false}},"view":false},"staff_management":{"label":"Manajemen Staf","sub_menu":{"1":{"label":"Data Staf","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false,"import":false,"print":false},"view":false},"2":{"label":"Posisi Staf","action":{"list":false,"create":false,"edit":false,"delete":false},"view":false},"3":{"label":"Kontrak Staf","action":{"download":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"4":{"label":"Deskripsi Pekerjaan Staf","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"5":{"label":"Benefit","action":{"data_list":false,"data_create":false,"data_edit":false,"data_delete":false,"staff_list":false,"staff_create":false,"staff_data_edit":false,"staff_data_delete":false},"view":false},"6":{"label":"Report","action":{"list":false},"view":false},"7":{"label":"Sallary","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"8":{"label":"Claim","action":{"wait_list":false,"wait_detail":false,"wait_confirm":false,"wait_reject":false,"confirm_detail":false,"reject_detail":false},"view":false},"9":{"label":"Learning & Development","action":{"wait_list":false,"wait_detail":false,"wait_confirm":false,"wait_reject":false,"confirm_detail":false,"reject_detail":false},"view":false},"10":{"label":"KPI","action":{"data_summary_list":false,"data_summary_data":false,"data_detail":false,"data_download":false,"data_create":false,"data_edit":false,"data_preview":false,"data_publish":false,"data_delete":false,"config_detail":false,"config_list":false,"config_create":false,"config_edit":false,"config_delete":false,"result_list":false,"result_create":false,"result_edit":false,"result_update":false},"view":false},"11":{"label":"Resign","action":{"list":false,"detail":false,"accept":false},"view":false}},"view":false},"staff_attendence":{"label":"Kehadiran Staf","sub_menu":{"1":{"label":"Jadwal Kerja","action":{"normal_list":false,"normal_update":false,"shift_view":false,"view_list":false,"view_create":false,"view_edit":false,"view_delete":false},"view":false},"2":{"label":"Day Off","action":{"view":false},"view":false},"3":{"label":"Absen","action":{"summary_list":false,"summary_detil":false,"manage_list":false,"manage_create":false,"manage_edit":false,"manage_delete":false,"publish_list":false,"publish_update":false,"import_do":false},"view":false},"4":{"label":"Leave","action":{"staff_wait_list":false,"staff_wait_confirm":false,"staff_wait_detail":false,"staff_wait_reject":false,"staff_approved_list":false,"staff_approved_detail":false,"staff_confirmed_list":false,"staff_confirmed_detail":false,"staff_canceled_list":false,"staff_canceled_detail":false,"staff_reject_list":false,"staff_reject_detail":false,"kuota_list":false,"kuota_edit":false,"leave_list":false,"leave_create":false,"leave_edit":false,"leave_delete":false,"manage_list":false,"manage_create":false,"manage_edit":false,"manage_delete":false},"view":false},"5":{"label":"Overtime","action":{"wait_list":false,"wait_confirm":false,"wait_detail":false,"wait_reject":false,"approve_list":false,"approve_detail":false,"confirm_list":false,"confirm_detail":false,"canceled_list":false,"canceled_detail":false,"reject_list":false,"reject_detail":false},"view":false}},"view":false},"general_data":{"label":"Manajemen Data Umum","sub_menu":{"1":{"label":"Pengumuman","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"2":{"label":"Berita","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"3":{"label":"Role & Permission","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"4":{"label":"Role User","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"7":{"label":"Banner","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false}},"view":false}},"staff":{"personal_menu":{"label":"Menu Personal","sub_menu":{"1":{"label":"Informasi Umum","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"2":{"label":"KPI","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"3":{"label":"Gaji","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false},"4":{"label":"Absence","action":{"view":false,"list":false,"create":false,"edit":false,"delete":false},"view":false}},"view":false},"key_activity":{"label":"Aktivitas Staf","sub_menu":{"1":{"label":"Daftar Approval","action":{"view":false,"update":false},"view":false},"2":{"label":"Laporan","action":{"view":false,"update":false},"view":false},"3":{"label":"Cuti","action":{"view":false,"update":false},"view":false},"4":{"label":"Claim","action":{"view":false,"update":false},"view":false},"5":{"label":"Overtime","action":{"view":false,"update":false},"view":false}},"view":false},"my_company":{"label":"Menu Perusahaan","sub_menu":{"1":{"label":"Profil Perusahaan","action":{"view":false,"update":false},"view":false},"2":{"label":"Alur Kerja","action":{"view":false,"update":false},"view":false},"3":{"label":"Aturan & Regulasi","action":{"view":false,"update":false},"view":false},"4":{"label":"Penghargaan","action":{"view":false,"update":false},"view":false},"5":{"label":"Hukuman","action":{"view":false,"update":false},"view":false},"6":{"label":"Pelatihan","action":{"view":false,"update":false},"view":false}},"view":false}}}';
    }

    public static function generate_staff_id_label($staff_id)
    {
        return 'STF-' . date('y') . $staff_id;
    }

    public static function get_file_type($filename)
    {
        $extension = pathinfo($filename, PATHINFO_EXTENSION);
        $type = 'image';
        if (in_array($extension, ['jpeg', 'jpg', 'png'])) {
            $type = 'image';
        } elseif (in_array($extension, ['pdf'])) {
            $type = 'pdf';
        } elseif(in_array($extension, ['xls','xlsx'])) {
            $type = 'excel';
        } elseif(in_array($extension, ['doc','docx'])) {
            $type = 'word';
        } elseif(in_array($extension, ['ppt','pptx'])) {
            $type = 'power_point';
        }

        return $type;
    }
}
