<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;

class rStaffStore implements ImplicitRule
{

    protected $username;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $this->username = $value;
        $check = mStaff::where('staff_username', $value)->count();
        if($check == 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username '.$this->username.' tidak tersedia';
    }
}