<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;

class rStaffUpdate implements ImplicitRule
{
    protected $staff_id;
    protected $username;

    public function __construct($staff_id)
    {
        $this->staff_id = $staff_id;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $staff_username_db = mStaff::where('staff_id', $this->staff_id)->value('staff_username');
        $staff_username = $value;
        if ($staff_username_db == $staff_username) {
            $status = TRUE;
        } else {
            $check = mStaff
                ::where('staff_username', $staff_username)
                ->whereNotIn('staff_username', [$staff_username_db])
                ->count();

            if($check > 0) {
                $this->username = $staff_username;
                $status = FALSE;
            } else {
                $status = TRUE;
            }
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username '.$this->username.' tidak tersedia';
    }
}