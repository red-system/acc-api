<?php

namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;

class rStaffPositionStore implements ImplicitRule
{

    protected $username;

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $status = FALSE;
        $staff_id = $value;

        $check = mStaff::where([
            'staff_id' => $staff_id,
        ])
            ->where('structure_id', '!=', '')
            ->count();
        if ($check == 0) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Posisi untuk staff ini sudah ada';
    }
}