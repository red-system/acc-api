<?php

namespace App\Rules;

use App\Models\mKPIResultCategory;
use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;
use Illuminate\Support\Facades\Hash;

class rKpiResultCategoryRange implements ImplicitRule
{
    protected $max;
    protected $min;

    public function __construct($max)
    {
        $this->max = $max;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string $attribute
     * @param  mixed $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $max = $this->max;
        $this->min = $value;
        $min = $value;

        $status = TRUE;

        $kpi_result_category = mKPIResultCategory::all();
        foreach ($kpi_result_category as $r) {
            $start = $r->kpi_rc_range_start;
            $end = $r->kpi_rc_range_end;

            if($min > $start && $max < $end) {
                echo $min.' > '.$start.' || '.$max.' < '.$end;
                $status = FALSE;
                break;
            }
        }

        /**
         * Login user
         */

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Rentang nilai salah atau tidak sesuai dengan rentang nilai yang sudah tersimpan '.$this->min.' = '.$this->max;
    }
}