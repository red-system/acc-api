<?php
namespace App\Rules;

use Illuminate\Contracts\Validation\ImplicitRule;
use App\Models\mStaff;
use Illuminate\Support\Facades\Hash;

class rLogin implements ImplicitRule
{
    protected $staff_username;

    public function __construct($staff_username)
    {
        $this->staff_username = $staff_username;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $staff_username = $this->staff_username;
        $staff_password = $value;
        $staff_password_db = mStaff::where('staff_username', $staff_username)->value('staff_password');
        $status = FALSE;

        /**
         * Login user
         */
        if (Hash::check($staff_password, $staff_password_db)) {
            $status = TRUE;
        }

        return $status;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Username atau Password salah';
    }
}