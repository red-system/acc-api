<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Http\Controllers\Controller;
use App\Models\mAcTransaksi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Transaksi extends Controller
{

    public function __construct()
    {

    }

    function index()
    {
        $data=mAcTransaksi::all();
        //General::log_activity('Banner', 'Melihat Banner', 'Melihat Data Banner', 'get');
        return General::response(General::$success, General::$get, $data);
    }

    function store(Request $request)
    {

        $data = $request->except('id_login');
        mAcTransaksi::create($data);
//        //General::log_activity('Banner', 'Menambah data', 'Menambah data Banner', 'insert');
        return General::response(General::$success, General::$update);
    }

    function row($id){
        $data=mAcTransaksi::find($id);
        //General::log_activity('Banner', 'Melihat data banner spesifik', 'Melihat data banner', 'get');
        return General::response(General::$success, General::$get, $data);
    }

    function update($id,Request $request)
    {
        $data = $request->except('id_login');
        mAcTransaksi::where('transaksi_id','=',$id)->update($data);

        // General::log_activity('Banner', 'Mengubah data', 'Mengubah banner', 'update');
        return General::response(General::$success, General::$update);
    }

    function delete($id)
    {
        mAcTransaksi::where(['transaksi_id' => $id])->delete();
        // General::log_activity('Banner', 'Menghapus data', 'Menghapus banner', 'delete');
        return General::response(General::$success, General::$delete);

    }


}


