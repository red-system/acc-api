<?php

namespace App\Http\Controllers\Acc;

use App\Helpers\General;
use App\Helpers\hAkunting;
use App\Http\Controllers\Controller;
use App\Models\mCoa;
use App\Models\mAcMaster;
use DemeterChain\Main;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class Coa extends Controller
{

    public function __construct()
    {

    }

    function index()
    {
        $space1 = hAkunting::perkiraan_space(1);
        $space2 = hAkunting::perkiraan_space(2);
        $space3 = hAkunting::perkiraan_space(3);
        $space4 = hAkunting::perkiraan_space(4);
        $perkiraan = [];

        $data1 = mAcMaster::where('mst_master_id', 0)
            ->orderBy('mst_kode_rekening', 'ASC')->get();
        foreach ($data1 as $k => $r) {
            $r->detail;
            $sub1 = mAcMaster::where('mst_master_id', $r->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
            $perkiraan[$k] = $r;
            $perkiraan[$k]['sub1'] = $sub1;
            foreach ($sub1 as $k1 => $r1) {
                $r1->detail;
                $sub2 = mAcMaster::where('mst_master_id', $r1->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                $perkiraan[$k]['sub1'][$k1]['sub2'] = $sub2;
                foreach ($sub2 as $k2 => $r2) {
                    $r2->detail;
                    $sub3 = mAcMaster::where('mst_master_id', $r2->master_id)->orderBy('mst_kode_rekening', 'ASC')->get();
                    $perkiraan[$k]['sub1'][$k1]['sub2'][$k2]['sub3'] = $sub3;
                    foreach ($sub3 as $k3 => $r3) {
                        $r3->detail;
                    }
                }
            }
        }
        $data = array();
        $data = array_merge($data, [
            'space1' => $space1,
            'space2' => $space2,
            'space3' => $space3,
            'space4' => $space4,
            'perkiraan' => $perkiraan
        ]);
        echo json_encode($data);
    }

    function store(Request $request)
    {
        $rules = [
            'kbt_kode_nama' => 'required',
            'kbt_keterangan' => 'required',
        ];

        $attributes = [
            'kbt_kode_nama' => 'COA title',
            'banner_description' => 'COA description',
        ];

        General::validator($request->all(), $rules, [], $attributes);
        $kbt_kode_nama= $request->input('kbt_kode_nama');
        $kbt_keterangan=$request->input('kbt_keterangan');

        $data= [
            'kbt_kode_nama' => $kbt_kode_nama,
            'kbt_keterangan' => $kbt_keterangan,
        ];
        mCoa::create($data);
        //General::log_activity('Banner', 'Menambah data', 'Menambah data Banner', 'insert');
        return General::response(General::$success, General::$update);
    }

    function row($id){
        $data=mCoa::find($id);
        //General::log_activity('Banner', 'Melihat data banner spesifik', 'Melihat data banner', 'get');
        return General::response(General::$success, General::$get, $data);
    }

    function update($id,Request $request)
    {
        $rules = [
            'kbt_kode_nama' => 'required',
            'kbt_keterangan' => 'required',
        ];

        $attributes = [
            'kbt_kode_nama' => 'COA title',
            'banner_description' => 'COA description',
        ];

        General::validator($request->all(), $rules, [], $attributes);
        $kbt_kode_nama= $request->input('kbt_kode_nama');
        $kbt_keterangan=$request->input('kbt_keterangan');

        $data= [
            'kbt_kode_nama' => $kbt_kode_nama,
            'kbt_keterangan' => $kbt_keterangan,
        ];
        mCoa::where('kode_bukti_id','=',$id)->update($data);

       // General::log_activity('Banner', 'Mengubah data', 'Mengubah banner', 'update');
        return General::response(General::$success, General::$update);
    }

    function delete($id)
    {
        mCoa::where(['kode_bukti_id' => $id])->delete();
       // General::log_activity('Banner', 'Menghapus data', 'Menghapus banner', 'delete');
        return General::response(General::$success, General::$delete);

    }
    function pembayaran(){
        $data = mAcMaster::where(['mst_pembayaran'=>'yes'])->get();
        return General::response(General::$success,General::$get,$data);
    }

}


