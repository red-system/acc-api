<?php

namespace App\Http\Controllers\General;

use App\Http\Controllers\Controller;
use App\Models\mStaff;
use App\Models\mRoleMaster;
use Illuminate\Http\Request;
use App\Rules\rLogin;
use App\Helpers\General;

class Login extends Controller
{

    public function __construct()
    {

    }

    function index(Request $request)
    {
        $username = $request->input('username');
        $rules = [
            'username' => 'required',
            'password' => ['required', new rLogin($username)]
        ];

        General::validator($request->all(), $rules);

        $token = General::generate_token();
        $data_where = [
            'staff_username' => $username
        ];
        $data_update = [
            'staff_token' => $token
        ];
        mStaff::where($data_where)->update($data_update);

        $staff = mStaff::with('position:structure_id,structure_name')
            ->where($data_where)
            ->first([
                'staff_id',
                'structure_id',
                'staff_name',
                'staff_avatar',
                'staff_register_complete',
                'staff_register_step',
                'staff_status',
                'roles_id_json',
                'roles_id',
            ]);

        // Filter jika staff belum aktif
        if ($staff->staff_status == 'not_active') {
            $errors = [
                'password' => ['staff tidak aktif']
            ];

            General::log_activity('Login', 'Login', 'Login User', 'insert', $staff->staff_id);
            return General::response(General::$error, General::$validation_error, NULL, $errors);
        }

        // Filter jika staff belum memiliki peran dalam sistem
        if ($staff->roles_id == '') {
            $errors = [
                'password' => ['staff belum memiliki peran dalam sistem']
            ];
            General::log_activity('Login', 'Login', 'Login User', 'get', $staff->staff_id);
            return General::response(General::$error, General::$validation_error, NULL, $errors);
        }

        // Filter apakah role user masih ada terdaftar dalam sistem
//        $roles_id_arr = json_decode($staff->roles_id_json, TRUE);
        $check_roles = mRoleMaster
            ::where('role_master_id', $staff->roles_id)
            ->count();
        if ($check_roles == 0) {
            $errors = [
                'password' => ['peran staff ini tidak terdaftar dalam sistem']
            ];
            General::log_activity('Login', 'Login', 'Login User', 'get', $staff->staff_id);
            return General::response(General::$error, General::$validation_error, NULL, $errors);
        }

        $roles = mRoleMaster::where('role_master_id', $staff->roles_id)->value('role_master_list');

        //$level_user = $this->_check_roles_level($roles_id_arr);

        $data = [
            'token' => $token,
            'staff' => $staff,
            'roles' => $roles,
//            'level_total' => count($level_user),
//            'level_list' => $level_user
        ];

        General::log_activity('Login', 'Login', 'Login User', 'insert', $staff->staff_id);
        return General::response(General::$success, General::$login_success, $data);


    }

    function _check_roles_level($roles_id_arr)
    {

        $role_master = mRoleMaster
            ::whereIn('role_master_id', $roles_id_arr)
            ->get();
        $level = [];
        $isHrd = false;
        $isStaff = false;

//        // Check, apakah user ini ada punya fitur HRD apa tidak
//        foreach ($role_master as $row) {
//            $role_master_hrd = json_decode($row->role_master_hrd, TRUE);
//            foreach ($role_master_hrd as $key => $row) {
//                foreach ($row as $key1 => $row1) {
//                    foreach ($row1 as $key2 => $row2) {
//                        foreach ($row2 as $key3 => $row3) {
//                            foreach ($row3 as $key4 => $row4) {
//                                if ($row4) {
//                                    $isHrd = true;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }
//
//        // Check, apakah user ini ada punya fitur STAFF apa tidak
//        foreach ($role_master as $row) {
//            $role_master_staff = json_decode($row->role_master_staff, TRUE);
//            foreach ($role_master_staff as $key => $row) {
//                foreach ($row as $key1 => $row1) {
//                    foreach ($row1 as $key2 => $row2) {
//                        foreach ($row2 as $key3 => $row3) {
//                            foreach ($row3 as $key4 => $row4) {
//                                if ($row4) {
//                                    $isStaff = true;
//                                    break;
//                                }
//                            }
//                        }
//                    }
//                }
//            }
//        }

//        if($isHrd) {
//            $level[] = 'hrd';
//        }
//
//        if($isStaff) {
//            $level[] = 'staff';
//        }


        return [
            'hrd','staff'
        ];
    }
}
