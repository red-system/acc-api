<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mAcMasterDetail extends Model
{
    protected $table = 'tb_ac_master_detail';
    protected $primaryKey = 'master_detail_id';
    protected $fillable = [
        'master_id',
        'msd_year',
        'msd_month',
        'msd_awal_kredit',
        'msd_awal_debet',
        'msd_date_insert',
        'msd_date_update',
        'created_at',
        'updated_at',
    ];
    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
