<?php

namespace app\Models;

use Illuminate\Database\Eloquent\Model;

class mAcTransaksi extends Model
{
    // public $incrementing = false;
    protected $datetime;
    protected $table = 'tb_ac_transaksi';
    protected $primaryKey = 'transaksi_id';
    protected $fillable = [
        'jurnal_umum_id',
        'id_distribusi',
        'master_id',
        'table_id',
        'table_name',
        'id_piutang_lain',
        'id_asset',
        'trs_jenis_transaksi',
        'tgl_transaksi',
        'trs_debet',
        'trs_kredit',
        'user_id',
        'trs_year',
        'trs_month',
        'trs_kode_rekening',
        'trs_nama_rekening',
        'trs_tipe_arus_kas',
        'trs_catatan',
        'trs_date_insert',
        'trs_date_update',
        'trs_charge',
        'trs_no_check_bg',
        'trs_tgl_pencairan',
        'trs_setor',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name'
    ];

    function __construct(array $attributes = [])
    {
        parent::__construct($attributes);

        $this->datetime = date('Y-m-d H:i:s');
    }

    function jurnal_umum() {
        return $this->belongsTo( mAcJurnalUmum::class, 'jurnal_umum_id');
    }

    function master() {
        return $this->belongsTo( mAcJurnalUmum::class, 'master_id');
    }
   
}
