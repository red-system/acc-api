<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mStaff extends Model
{
    protected $table = 'staff';
    protected $primaryKey = 'staff_id';
    protected $hidden = ['staff_password'];
    protected $fillable = [
        'structure_id',
        'roles_id_json',
        'roles_id',
        'staff_id_label',
        'staff_name',
        'staff_nickname',
        'staff_avatar',
        'staff_work_start',
        'staff_quota_leave',
        'staff_username',
        'staff_password',
        'staff_status',
        'staff_nationality',
        'staff_gender',
        'staff_birth_place',
        'staff_birth_date',
        'staff_identity_type',
        'staff_identity_number',
        'staff_religion',
        'staff_marital_status',
        'staff_primary_address',
        'staff_primary_province',
        'staff_primary_district',
        'staff_primary_post_code',
        'staff_second_address',
        'staff_second_province',
        'staff_second_district',
        'staff_second_post_code',
        'staff_phone_number',
        'staff_telephone_number',
        'staff_email',
        'staff_phone_emergency',
        'staff_register_complete',
        'staff_register_step',
        'created_staff_id',
        'updated_staff_id',
    ];

    protected $appends = ['staff_avatar_url', 'staff_work_total_time'];

    public function getStaffAvatarUrlAttribute()
    {
        return url(env('PATH_STAFF_AVATAR'). $this->staff_avatar);
    }

    public function getStaffWorkTotalTimeAttribute()
    {
        $fromYear = date('Y', strtotime($this->staff_work_start));
        $fromMonth = date('m', strtotime($this->staff_work_start));
        $fromDay = date('d', strtotime($this->staff_work_start));
        $toYear = date('Y');
        $toMonth = date('m');
        $toDay = date('d');

        $datetime1 = date_create($fromYear . '-' . $fromMonth . '-'.$fromDay);// Concatenate & consider date starts from 1st
        $datetime2 = date_create($toYear . '-' . $toMonth . '-'.$toDay);

        $interval = date_diff($datetime1, $datetime2);

        if($interval->y == 0) {
            return $interval->m.' bulan';
        } else {
            return $interval->y . ' tahun, ' . $interval->m . ' bulan';
        }
    }
    public function kpi()
    {
        return $this->belongsTo(mKPIStaff::class, 'staff_id', 'staff_id');
    }
    public function position()
    {
        return $this->belongsTo(mStructure::class, 'structure_id', 'structure_id');
    }
    public function contract()
    {
        return $this->belongsTo(mStaffContract::class, 'staff_id', 'staff_id');
    }
    public function primary_province(){
        return $this->belongsTo(mProvince::class, 'staff_primary_province_id', 'province_id');
    }
    public function secondary_province(){
        return $this->belongsTo(mProvince::class, 'staff_second_province_id', 'province_id');
    }
    public function primary_district(){
        return $this->belongsTo(mDistrict::class, 'staff_primary_district_id', 'district_id');
    }
    public function secondary_district(){
        return $this->belongsTo(mDistrict::class, 'staff_second_district_id', 'district_id');
    }
    public static function create(array $data = [])
    {
        $staff = General::user_detail();
        $staff_id = $staff->staff_id;

        $data['created_staff_id'] = $staff_id;

        $model = static::query()->create($data);
        return $model;
    }

    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }

    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
