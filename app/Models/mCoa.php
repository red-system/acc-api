<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mCoa extends Model
{
    protected $table = 'tb_ac_kode_bukti';
    protected $primaryKey = 'kode_bukti_id';
    protected $fillable = [
        'kbt_kode_nama',
        'kbt_kode_nama',
        'kbt_keterangan',
        'created_at',
        'created_username',
        'created_id',
        'updated_at',
        'updated_username',
        'updated_id',
    ];
    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
