<?php

namespace App\Models;

use App\Helpers\General;
use Illuminate\Database\Eloquent\Model;

class mJurnalUmum extends Model
{
    protected $table = 'tb_ac_jurnal_umum';
    protected $primaryKey = 'jurnal_umum_id';
    protected $fillable = [
        'no_invoice',
        'table_id',
        'table_name',
        'id_supplier',
        'id_history_penyesuaian_bahan',
        'id_history_penyesuaian_produk',
        'id_penjualan',
        'id_distributor',
        'id_karyawan',
        'id_asset',
        'id_penyusutan_asset',
        'id_hutang_supplier',
        'id_hutang_lain',
        'id_piutang_lain',
        'id_piutang_pelanggan',
        'id_distribusi',
        'id_po_bahan',
        'id_produksi',
        'id_progress_produksi',
        'pelaku_id',
        'pelaku_table',
        'jmu_tanggal',
        'jmu_no',
        'jmu_keterangan',
        'jmu_year',
        'jmu_month',
        'jmu_day',
        'created_by',
        'updated_by',
        'created_by_name',
        'updated_by_name',
        'edit',
        'reference_number',
        'created_at',
        'updated_at',
    ];
    public function getCreatedAtAttribute()
    {
        return date(General::$date_format_view, strtotime($this->attributes['created_at']));
    }
    public function getUpdatedAtAttribute()
    {
        return \Carbon\Carbon::parse($this->attributes['updated_at'])
            ->diffForHumans();
    }


}
